# Schemas for GitLab security reports

This repository defines the schemas for the security reports emitted by GitLab security scanners. It defines the reports for:
- [Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)
- [Cluster Image Scanning](https://docs.gitlab.com/ee/user/application_security/cluster_image_scanning/)
- [Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/)
- [Static Application Security Testing](https://docs.gitlab.com/ee/user/application_security/sast/)
- [Dynamic Application Security Testing](https://docs.gitlab.com/ee/user/application_security/dast/)
- [Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/)
- [Coverage Guided Fuzzing](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/)

The schemas are defined using [JSON Schema](https://json-schema.org/). Any security scanner that integrates into GitLab must produce a JSON report that adheres to one of above schemas.

More information about the how and why of the schemas can be found by watching the [Security Report Format Brown Bag Session](https://youtu.be/DqKsdNLXxes).

## Who owns the schema?
All groups in the Sec Section jointly own the schema.

### Who should I assign my MR to for review?
Many changes to the schema are not [two-way door decisions](https://about.gitlab.com/handbook/values/#make-two-way-door-decisions).
Once a schema is released, analyzers will adopt it, and GitLab Rails will enforce schema validation. It is hard to walk back from these changes.
Please keep this in mind and cast the net wider than usual when asking for a code review.

#### Guidelines
- [Threat Insights](https://about.gitlab.com/handbook/engineering/development/sec/govern/threat-insights/) MUST review all changes as they are the consumer of reports that conform to the schema. They are affected most by changes as GitLab Rails needs to support multiple schema versions.
  - If Threat Insights is making the change, then a [representative of the architecture council](https://about.gitlab.com/handbook/engineering/development/sec/#team-representatives) from any other group MUST review, either as a maintainer or reviewer.
- A change to an analyzer specific schema MUST involve a person from the group responsible for that analyzer.
- A change that is a conceptual change to the way something works SHOULD involve the [Secure Architecture Council](https://about.gitlab.com/handbook/engineering/development/sec/#architectural-council-slack-s_sec-architectural-council).
  - At the very least, please communicate this change to the whole Sec Section before merging the MR.

### How to become a maintainer

See [Becoming a maintainer](CONTRIBUTING.md#becoming-a-maintainer).

### How do we resolve disputes?
Please create an issue and raise the dispute with the [Secure Architecture Council](https://about.gitlab.com/handbook/engineering/development/sec/#architectural-council-slack-s_sec-architectural-council).

## External Integration

Secure schemas are published to external package management systems:

<details>
<summary><font size="3">NPM</font></summary>

GitLab.com provides a package repository per repository which supports JavaScript package manager integration.  To install the 
Security schema module via `npm`:

1. Add the `@gitlab-org` registry location to your project's `.npmrc` configuration file.

    ```sh
    npm config set --userconfig ./.npmrc @gitlab-org:registry https://gitlab.com/api/v4/packages/npm/
    ```

2. Install via `npm install --save @gitlab-org/security-report-schemas`.
</details>

## How to modify a schema
Schemas are made from a combination of the base `security-report-format.json` schema and an extension schema that belongs to a report type (e.g. `sast-report-format.json`). These files can be found in the `src` directory of the project.

### Determining where the change belongs 
When adding new fields to a schema, please add it to the appropriate place:

- If a field is experimental, it likely belongs in the specific report schema
- If a field is used by few report types, it likely belongs in each report type schema
- If a field is to be used by all report types, it likely belongs in the base schema

### Adding a new field to an extension schema

New fields in extension schemas must be added to the `allOf` array in the source schema file. 
For example, if DAST was to extend the vulnerability definition, and add a new required field called `evidence`:

```json
 "allOf": [
    { "$ref": "security-report-format.json" },
    {
      "properties": {
        "vulnerabilities": {
          "items": {
            "properties": { "evidence": { "type": "string" } },
            "required": [ "evidence" ]
          }
        }
      }
    }
  ]
```

Adding the new field to the normal `properties` field in the extension schema, or redefining `definitions` with the new field definition will not result in the field being contained in the generated schema output.

When adding a new field, please add an appropriate check in the `tests/test-*.sh` file to verify that the merge contains the field you expect.

### Deprecating a field

If you want to deprecate a field from any of the schemas, please create an issue using the `Field deprecation` template. Field deprecations have impact on GitLab itself so there are several steps we need to follow.

### Building for extensibility

Keep in mind that as the schemas grow fields will need to be altered to add more content. While the future cannot be predicted, the schemas can be made easy to change with some simple design choices.

For example, the `vendor` property in the following snippet cannot be extended, because it is of type `string`:  
```json
"vendor": { "type": "string" }
```

This can be improved by making `vendor` an `object`.
```json
"vendor": {
    "type": "object",
    "required": ["name"],
    "properties": {
      "name": { "type": "string" }
    }
  }
```

Similarly, each link in the following example cannot be extended: 

```json
"links": {
  "type": "array"
  "items": { "type": "string" }
}
```

This can be improved by making each item in the array an object.

```json
"links": {
  "type": "array",
  "items": {
    "type": "object",
    "required": ["url"],
    "properties": {
      "url": { "type": "string" }
    }
  }
}
```

### String properties with length limits

Properties of `"type": "string"` that are ingested by the `gitlab-org/gitlab` application should always have a 
documented `maxLength` limit.

In these cases, try to use the same value as the corresponding Rails validations; e.g.: `::Issuable::TITLE_LENGTH_MAX`,
`::Issuable::DESCRIPTION_LENGTH_MAX`.

When these limits are updated in Rails, they must be updated here as well.

### Pre-commit expectations

Schemas are released by unifying the base schema with each report schema. All the `$ref` and `allOf` references are inlined, duplicate fields are merged, and the output is written to the `dist` directory.

To ensure that a successful run of the CI Pipeline on the `master` branch of the project can be released,
engineers must do the following:
 
- The CHANGELOG version should be incremented, and a description should be added. Please follow a consistent format.
- The `src/security-report-format.json` `self.version` field should be updated to the same version as the new CHANGELOG entry 
- The `dist` output should be regenerated and checked-in. This can be achieved by (or check the docker alternative below):
  - Install `node`, `npm`, and `jq`
  - Run `npm install` from the project directory
  - Run `./scripts/distribute.sh`
- Alternatively, you can use the provided `Dockerfile`:
  - `docker build -t security-report-schemas .`
  - `docker run -it --rm -v $PWD:/security-report-schemas security-report-schemas ./scripts/distribute.sh`

## How to release a new version of the schema

To release a new version of the schema, trigger the manual release job at the end of a successful master CI Pipeline.

Please coordinate and communicate with others when triggering a release. Prior to publishing a release, we may want to merge and include updates from multiple branches into the `main` branch. Look at other open MRs and changes that have not been released to get an idea of whether those might need to be included and coordinate with their authors.
Releases of the schema do not need to correspond to GitLab releases, in fact, there is no limit as to how often releases can be made.

### RubyGem release

1. For commits to the default branch, the `release` job can be triggered to initiate a release. A new release is created on the GitLab instance from the most recent changelog version.
2. The `deploy-gem` job triggers a downstream pipeline in the [security-report-schemas-ruby](https://gitlab.com/gitlab-org/ruby/gems/gitlab-security_report_schemas) project.
  1. The new security report schema version (tag) is added to the list of `supported_version`.
  2. The gem version is bumped, the change committed, tagged with the version and pushed.
  3. A new rubygems.org version is released.
  4. A merge request to gitlab-org/gitlab is created, which updates the `Gemfile` to use the newly released version.

### What happens when the schemas are released?

Releasing does the following:

1. Reads the most recent CHANGELOG entry to determine the version and description.
1. Verifies that the version has not already been released.
1. Creates a Git tag for the version, pointing to the commit SHA that ran on the master pipeline.
1. Creates a [GitLab release](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/releases) based on the new Git tag.
1. Adds release notes containing CHANGELOG description, and links to download the schemas.
1. Creates a schemas-only package and publishes it to the [GitLab NPM repository](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/packages).
  * Creates a Merge Request to add the new schemas into the GitLab Rails code repository ([example](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/75547)). The MR is assigned to a person who commits to getting the MR reviewed and merged.
  * Triggers a downstream release pipeline in the [security-report-schemas-ruby](https://gitlab.com/gitlab-org/ruby/gems/gitlab-security_report_schemas) project. The downstream pipeline publishes a new version of `gitlab-security_report_schemas` to rubygems.org and creates a Merge Request that updates the `Gemfile`'s dependency version.

### Can I merge an MR and prevent it from being immediately released?

Setting the CI/CD variable `BLOCK_RELEASE` with any non-empty value will halt a release as soon as it is triggered. This is a global setting, all releases are affected when this variable is set.

Please set/clear the variable in the project CI/CD variable settings so that changes can be made without involving source control.

The contents of the variable is printed on the failed release job log, for this reason please include your name and the
reason releasing has been blocked.

## Versioning schemas

This repository follows the [SchemaVer](https://snowplow.io/blog/introducing-schemaver-for-semantic-versioning-of-schemas/) standard to version JSON schemas.

### Classifying changes

For the following situations, you MUST increment the `MODEL.REVISION.ADDITION` version as suggested. You SHOULD refer back to
SchemaVer if your use-case is not listed.

- If you add a new optional field, whether or not it contains required fields, you MUST use an `ADDITION` change.
- If you change an existing field to be required, or add a new required field, you MUST use a `MODEL` change.
- If you remove a field, you MUST use a `REVISION` change.

The use of a `MODEL` change for new required fields will result in many new `MODEL` releases. This is normal when
using SchemaVer, and does not prohibit products that depend on the Secure Report Format from being explicit regarding versions
they support.

### Additional Properties

Secure schemas allow for additional properties to be present in JSON files. This means that the schemas are only concerned with
fields in a Secure Report that are defined by the schema.
The presence of any additional fields will not cause validation to fail.

This is useful for products that produce Secure Reports:

- Experimental fields can be added to a Secure Report, without affecting how the report is used.
- It allows the product to be ahead of the Secure Report Format, when the product team is confident new fields will be merged into the schemas.

Any additional properties added to a Secure Report are considered experimental and may not be supported. For this reason,
adding optional fields to the Secure Report Format is considered an `ADDITION`, not a `REVISION` change.


## Testing

Tests are used to verify that the appropriate fields are present in the Secure schemas.
The tests are implemented using the [`bash_unit`](https://github.com/pgrange/bash_unit) framework.

To run the tests, follow these steps (or use the docker way below):

1. [Install `bash_unit`](https://github.com/pgrange/bash_unit#other-installation) on Mac OS:

    ```bash
    bash <(curl -s https://raw.githubusercontent.com/pgrange/bash_unit/master/install.sh)
    mv ./bash_unit /usr/local/bin
    ```

1. Install [`jq`](https://stedolan.github.io/jq/) on Mac OS:

    ```bash
    brew install jq
    ```

1. Install the required npm modules

    ```bash
    npm install
    ```

1. Run the tests

    ```bash
    ./test/test.sh
    ```

### Testing with Docker

After building the image from the provided `Dockerfile`:

```
docker run -it --rm -v $PWD:/security-report-schemas security-report-schemas ./test/test.sh
```

## Environment variables

| Key                       | Description                                                                                                                       |
|---------------------------|-----------------------------------------------------------------------------------------------------------------------------------|
| `DANGER_GITLAB_API_TOKEN` | PAT with access to the project.                                                                                                   |
| `GITLAB_API_TOKEN`        | PAT with access to the project.                                                                                                   |
| `PIPELINE_TRIGGER_TOKEN`  | Pipeline trigger token for [security-report-schemas-ruby](https://gitlab.com/gitlab-org/ruby/gems/gitlab-security_report_schemas) |
|                           |                                                                                                                                   |

## Submit a bug or enhancement suggestion
Please follow the steps in our [Contributor doc](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/CONTRIBUTING.md#issue-tracker). 

## Contributing
If you want to help and extend the list of supported scanners, read the
contribution guidelines https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/CONTRIBUTING.md
