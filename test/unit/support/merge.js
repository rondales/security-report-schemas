
export const merge = (...values) => {
  const merged = values.reduce((combined, value) => Object.assign(combined, value), {})

  // remove functions, undefined values, etc from result
  return JSON.parse(JSON.stringify(merged))
}
