#!/bin/bash

PROJECT_DIRECTORY=$(realpath "$(dirname "$(realpath "${BASH_SOURCE[0]}")")/..")
CS_SCHEMA="$PROJECT_DIRECTORY/dist/container-scanning-report-format.json"

source "$PROJECT_DIRECTORY/test/helper_functions.sh"
source "$PROJECT_DIRECTORY/test/common-tests.sh"

setup_suite() {
  regenerate_dist_schemas
}

test_container_scanning_contains_common_definitions() {
  ensure_common_definitions "$CS_SCHEMA" '["container_scanning", "container_scanning_for_registry"]'
}

test_container_scanning_extensions() {
  verify_schema_contains_selector "$CS_SCHEMA" 'select(.properties.vulnerabilities.items.required[] | contains("location"))'
  verify_schema_contains_selector "$CS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.image"
  verify_schema_contains_selector "$CS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.default_branch_image"
  verify_schema_contains_selector "$CS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.operating_system"
  verify_schema_contains_selector "$CS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.dependency.properties.package.properties.name"
  verify_schema_contains_selector "$CS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.dependency.properties.version"
  verify_schema_contains_selector "$CS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.dependency.properties.package"
}
