#!/bin/bash

PROJECT_DIRECTORY=$(realpath "$(dirname "$(realpath "${BASH_SOURCE[0]}")")/..")
SAST_SCHEMA="$PROJECT_DIRECTORY/dist/sast-report-format.json"

source "$PROJECT_DIRECTORY/test/helper_functions.sh"
source "$PROJECT_DIRECTORY/test/common-tests.sh"

setup_suite() {
  regenerate_dist_schemas
}

test_sast_contains_common_definitions() {
  ensure_common_definitions "$SAST_SCHEMA" '["sast"]'
}

test_sast_extensions() {
  verify_schema_contains_selector "$SAST_SCHEMA" 'select(.properties.vulnerabilities.items.required[] | contains("location"))'

  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.file"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.start_line"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.end_line"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.method"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.class"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.raw_source_code_extract"
}
